//
//  ViewController.swift
//  createContactApp
//
//  Created by Mounika Nerella on 8/28/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class ViewController: UIViewController {
    
    @IBOutlet weak var confrmPswdtxtFld: UITextField!
    @IBOutlet weak var passwordtxtFld: UITextField!
    @IBOutlet weak var emailidtxtfld: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    @IBAction func loginBtn(_ sender: UIButton) {
    }

    @IBAction func SignUpbtn(_ sender: UIButton) {
        let mail = emailidtxtfld.text
        let password = passwordtxtFld.text
        let confrmPasswrd = confrmPswdtxtFld.text
        
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            
        }
        if password! == confrmPasswrd! {
            Auth.auth().createUser(withEmail: mail!, password: password!) { (user, error) in
                if let error = error {
                    
                    print(error.localizedDescription)
                    return
                }
                print("User Created")
                if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DisplayTableViewController") as? DisplayTableViewController{
                    
                    self.navigationController?.pushViewController(controller, animated: true)
                    self.passwordtxtFld.text = ""
                    self.confrmPswdtxtFld.text = ""
                    
                }
            }
        }

//                if error == nil {
//                    let alertController = UIAlertController(title: "Alert", message: "Successfully added User", preferredStyle: .alert)
//                    alertController.addAction(alertAction)
//                    self.present(alertController, animated: true, completion: nil)
//                }
//                else {
//                    
//                    let alertController = UIAlertController(title: "Alert", message: error?.localizedDescription ?? "No Error Description", preferredStyle: .alert)
//                    alertController.addAction(alertAction)
//                    self.present(alertController, animated: true, completion: nil)
//                    
//                }
//                
//            }
//
//        }
//    }
//    
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

