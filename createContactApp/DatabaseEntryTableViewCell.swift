//
//  DatabaseEntryTableViewCell.swift
//  createContactApp
//
//  Created by Mounika Nerella on 8/28/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import FirebaseDatabase
import FirebaseAuth

class DatabaseEntryTableViewCell: UITableViewCell {

    @IBOutlet weak var nameTxt: UILabel!
   
    @IBOutlet weak var addInfo: UIButton!
    @IBOutlet weak var personImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

       override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
