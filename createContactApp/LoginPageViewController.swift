//
//  LoginPageViewController.swift
//  createContactApp
//
//  Created by Mounika Nerella on 8/28/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import FirebaseAuth

class LoginPageViewController: UIViewController {
    
    @IBOutlet weak var pswdtxt: UITextField!
    @IBOutlet weak var emailtxt: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Do any additional setup after loading the view.
    }
    @IBAction func loginbtn(_ sender: UIButton) {
        let mail = emailtxt.text
        let pass = pswdtxt.text
        
        Auth.auth().signIn(withEmail: mail!, password: pass!) { (user, error) in
            
            if let error = error {
                print(error.localizedDescription)
                self.pswdtxt.text = ""
                self.emailtxt.text = ""
                return
            }
            print("User logged in")
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DisplayTableViewController") as? DisplayTableViewController {
                self.navigationController?.pushViewController(controller, animated: true)
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
