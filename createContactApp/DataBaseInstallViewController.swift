//
//  DataBaseInstallViewController.swift
//  createContactApp
//
//  Created by Mounika Nerella on 8/28/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class DataBaseInstallViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var contactImg: UIImageView!
    @IBOutlet weak var agetxt: UITextField!
    @IBOutlet weak var ssntxt: UITextField!
    @IBOutlet weak var lnametxt: UITextField!
    @IBOutlet weak var fnamtxt: UITextField!
    var info: Array<Details> = []
    let storage = Storage.storage()
    @IBOutlet weak var tblView: UITableView!
    var ref: DatabaseReference!
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        // Do any additional setup after loading the view.
    }
    @IBAction func imageBtn(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    @IBAction func addBtn(_ sender: UIButton) {
    
        guard let firstname = fnamtxt.text,let lastname = lnametxt.text,let ssn = ssntxt.text,let age = agetxt.text else {
            print(" The field is empty")
            return
        }
        
         ref.child("users").child((Auth.auth().currentUser?.uid)!).setValue(["firstName":firstname,"lastName":lastname,"age":age,"ssn":ssn])
        print((Auth.auth().currentUser?.uid)!)
        ref.child("publicUsers").child((Auth.auth().currentUser?.uid)!).setValue(["firstName":firstname,"lastName":lastname,"age":age,"ssn":ssn])
        uploadImage()
        navigationController?.popViewController(animated: true)
        
//   ssntxt.text = ""
//     agetxt.text = ""
//       lnametxt.text = ""
//       fnamtxt.text = ""
    

    }
    func uploadImage(){
        let imageData = UIImageJPEGRepresentation(contactImg.image!, 1)
        let storageRef = storage.reference()
        let uid = Auth.auth().currentUser!.uid
        let imageRef =  storageRef.child("images/\(uid).jpg")
        let _ = imageRef.putData(imageData!,metadata: nil)
        {(metadata,error) in
            guard let _ = metadata else{
                print("error")
                return
            }
            print("image uploaded")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print("image selected")
        
        let img = info[UIImagePickerControllerEditedImage] as? UIImage
        
        
        //        }
        //    else{
        //            print("something went wrong")
        //        }
        self.dismiss(animated: true, completion: nil)
        contactImg.image = img
    }
    
    func getDetails() {
        let refHandle = ref.observe(DataEventType.value, with: { (snapshot) in
            
            self.ref.child("users").observeSingleEvent(of: .value, with: { (snapshot) in
                let value = snapshot.value as? [String:Any]
                for user in (value as? [String:Any])! {
                    
                 //   print(user["firstName"])
                    
                }
            }) { (error) in
                print(error.localizedDescription)
            }
            
        })
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
