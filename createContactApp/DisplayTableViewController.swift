//
//  DisplayTableViewController.swift
//  createContactApp
//
//  Created by Mounika Nerella on 8/29/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage

class DisplayTableViewController: UITableViewController {
    var ref: DatabaseReference!
    var infoOfFrnds = [Details]()
    let storage = Storage.storage()
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference()
        readUsers()
        tableView.tableFooterView = UIView()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
    }
    func readUsers(){
        self.ref.child("publicUsers").observeSingleEvent(of: .value, with: { (snapshot) in
        
            guard let users = snapshot.value as? Dictionary<String,[String:Any]> else { return }
            for user in users {
                let names = user.value
                if user.key != Auth.auth().currentUser?.uid {
                    
                    if let fName = names["firstname"], let lName = names["lastname"] {
                    self.infoOfFrnds.append(Details(fname: fName as! String, lname: lName as! String, uid: user.key))
                    }
                }
            }
            self.tableView.reloadData()
    })
    }
   
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
            let firebaseAuth = Auth.auth()
        do{
            try firebaseAuth.signOut()
        }catch let signOutError as NSError{
            print("Error signing out: %@",signOutError)
            return
        }
        print(" User signed out")
        navigationController?.popToRootViewController(animated: true)
    }
   
    @IBAction func updateInfo(_ sender: UIBarButtonItem) {
            if let controller = self.storyboard?.instantiateViewController(withIdentifier: "DataBaseInstallViewController") as? DataBaseInstallViewController{
            navigationController?.pushViewController(controller, animated: true)
        }
    }
    func addInfo(sender: UIButton){
        let array1 = infoOfFrnds[sender.tag]
        let arrayUid = array1.uid11
        let currentUid = Auth.auth().currentUser?.uid
        ref.child("publicUsers/\(currentUid)/friends/\(arrayUid)").setValue(true)
        print("friend added")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return infoOfFrnds.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DEtableViewCell", for: indexPath) as? DatabaseEntryTableViewCell
        let obj = infoOfFrnds[indexPath.row]
        let uid = obj.uid11
        let storageRef = storage.reference()
        let imageRef = storageRef.child("images/\(uid).jpg")
        imageRef.getData(maxSize: 1 * 2048 * 1024) { data,error in
            if let error = error{
                print(error.localizedDescription)
                
            }else{
                cell?.personImage?.image = UIImage(data: data!)
                print("got Image")
            }
        }
        cell?.nameTxt.text = obj.firstName+" " + obj.lstname
        let addBtn = cell?.addInfo
        addBtn?.tag = indexPath.row
        addBtn?.addTarget(self, action: #selector(addInfo), for: .touchUpInside)
        

        return cell!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
