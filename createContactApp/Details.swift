//
//  Details.swift
//  createContactApp
//
//  Created by Mounika Nerella on 8/28/17.
//  Copyright © 2017 Mounika Nerella. All rights reserved.
//

import Foundation
class Details{
    var firstName: String
    var lstname: String
    var uid11: String
    
    init(fname: String,lname: String,uid: String){
        firstName = fname
        lstname = lname
        uid11 = uid
        
    }
}
